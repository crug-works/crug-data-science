# CRUG Data Science

![CRUG logo](crug_skyline.png)

## Repository for CRUG scripts and projects involving data analytics and data science:

- ### Artificial Intelligence
- ### Big Data and Aggregation
- ### Empirical and Predictive Modeling
- ### Machine Learning
- ### Spatial Analysis
- ### Statistical Methods

